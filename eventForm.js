"sue strict";

function showAlert(text) {
    console.log(text)
}

function listFormElement() {
    var formElements = document.getElementsByTagName('form')
    console.log(formElements)
}

function copyTextFromNameToLastName() {
    var nameElement = document.getElementById('Firstname')
    var surnameElement = document.getElementById('Lastname')
    var firstname = nameElement.value
    console.log('Firstname is ' + firstname)
    console.log('now lastname is ' + surnameElement.value)
    surnameElement.value = firstname
}

function clearData() {
    var clearnameElemmt = document.getElementById('Firstname')
    var clearsurnameElement = document.getElementById('Lastname')
    var clearEmailElement = document.getElementById('InputEmail')
    var clearPassElement = document.getElementById('inputpass')
    var clearConPassElement = document.getElementById('inputconpass')
    var clearStatus1 = document.getElementById('Radios1')
    var clearStatus2 = document.getElementById('Radios2')
    var clearCheckbok = document.getElementById('gridCheck1')

    clearnameElemmt.value = '';
    clearsurnameElement.value = '';
    clearEmailElement.value = '';
    clearPassElement.value = '';
    clearConPassElement.value = '';
    clearStatus1.checked = false;
    clearStatus2.checked = false;
    clearCheckbok.checked = false;
}
function sendstatus() {
    var status1 = document.getElementById('Radios1').value
    var status2 = document.getElementById('Radios2').value

    var RadiosEmail;
    if (document.getElementById('Radios1').checked == true) {
        RadiosEmail = status1
    } else if (document.getElementById('Radios2').checked == true) {
        RadiosEmail = status2
    }
    console.log(RadiosEmail)
    document.getElementById('InputEmail').value = RadiosEmail

}
function showSummary() {
    var name = document.getElementById('Firstname').value
    var lastname = document.getElementById('Lastname').value
    var emil = document.getElementById('InputEmail').value
    var pass = document.getElementById('inputpass').value
    var conpass = document.getElementById('inputconpass').value
    var summaryElement = document.getElementById('summary')

    if (name != '') {
        summaryElement.innerHTML = (name + ' ' + lastname + '<br>' + emil + '<br>' + pass + '<br>' + conpass)
        summaryElement.removeAttribute('hidden')

    } else if (lastname != '') {
        summaryElement.innerHTML = (name + ' ' + lastname + '<br>' + emil + '<br>' + pass + '<br>' + conpass)
        summaryElement.removeAttribute('hidden')
    } else if (emil != '') {
        summaryElement.innerHTML = (name + ' ' + lastname + '<br>' + emil + '<br>' + pass + '<br>' + conpass)
        summaryElement.removeAttribute('hidden')
    }
    else if (pass != '') {
        summaryElement.innerHTML = (name + ' ' + lastname + '<br>' + emil + '<br>' + pass + '<br>' + conpass)
        summaryElement.removeAttribute('hidden')
    } else if (conpass != '') {
        summaryElement.innerHTML = (name + ' ' + lastname + '<br>' + emil + '<br>' + pass + '<br>' + conpass)
        summaryElement.removeAttribute('hidden')
    } else {
        summaryElement.hidden = true
    }

}

function addPElement() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
}

function addHref() {
    var node = document.createElement('a')
    node.innerHTML = 'google'
    node.setAttribute('href', 'https://www.google.co.th')
    node.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
}

function addBoth() {
    var node = document.createElement('p')
    node.innerHTML = 'Hello world'
    var node2 = document.createElement('a')
    node2.innerHTML = 'google'
    node2.setAttribute('href', 'https://www.google.co.th')
    node2.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder')
    placeHolder.appendChild(node)
    placeHolder.appendChild(node2)
}

function showform() {
    var node = document.createElement('p')
    node.innerHTML = 'Please fill in this form to create account'

    var node1 = document.createElement('input')
    node1.setAttribute('type', 'text')
    node1.setAttribute('id', 'firstname')
    node1.setAttribute('class', 'form-control')
    node1.setAttribute('placeholder', 'First name')


    var node2 = document.createElement('input')
    node2.setAttribute('type', 'text')
    node2.setAttribute('id', 'lastname')
    node2.setAttribute('class', 'form-control')
    node2.setAttribute('placeholder', 'Last name')



    var node3 = document.createElement('input')

    node3.setAttribute('type', 'email')
    node3.setAttribute('id', 'Email')
    node3.setAttribute('class', 'form-control')
    node3.setAttribute('placeholder', 'Email')


    var divform = document.createElement('from')

    var divformrow = document.createElement('div')
    divformrow.setAttribute('class', 'form-row')


    var divformgroup1 = document.createElement('div')
    divformgroup1.setAttribute('class', ' form-group col-6')

    var divformgroup2 = document.createElement('div')
    divformgroup2.setAttribute('class', 'form-group col-6')

    var divformgroup3 = document.createElement('div')
    divformgroup3.setAttribute('class', 'form-group')

    var placeHolder = document.getElementById('placehoder0')

    placeHolder.appendChild(divform)
    divform.appendChild(node)
    divform.appendChild(divformrow)
    divform.appendChild(divformgroup3)

    divformrow.appendChild(divformgroup1)
    divformrow.appendChild(divformgroup2)

    divformgroup1.appendChild(node1)
    divformgroup2.appendChild(node2)
    divformgroup3.appendChild(node3)
}

function tableA() {
    var mainTable = document.createElement('table')
    mainTable.setAttribute('class', 'table table-striped')

    var thead = document.createElement('thead')
    var tr1 = document.createElement('tr')

    var th1 = document.createElement('th')
    th1.setAttribute('scope', 'col')
    th1.innerHTML = '#'

    var th2 = document.createElement('th')
    th2.setAttribute('scope', 'col')
    th2.innerHTML = 'First'

    var th3 = document.createElement('th')
    th3.setAttribute('scope', 'col')
    th3.innerHTML = 'Last'

    var th4 = document.createElement('th')
    th4.setAttribute('scope', 'col')
    th4.innerHTML = 'Handle'

    var tbody = document.createElement('tbody')

    var tr2 = document.createElement('tr')
    var th5 = document.createElement('th')
    th5.setAttribute('scope', 'row')
    th5.innerHTML = '1'

    var td1 = document.createElement('td')
    td1.innerHTML = 'Mark'

    var td2 = document.createElement('td')
    td2.innerHTML = 'Otto'

    var td3 = document.createElement('td')
    td3.innerHTML = '@mdo'


    var tr3 = document.createElement('tr')
    var th6 = document.createElement('th')
    th6.setAttribute('scope', 'row')
    th6.innerHTML = '2'

    var td4 = document.createElement('td')
    td4.innerHTML = 'Jacob'

    var td5 = document.createElement('td')
    td5.innerHTML = 'Thornton'

    var td6 = document.createElement('td')
    td6.innerHTML = '@fat'


    var tr4 = document.createElement('tr')
    var th7 = document.createElement('th')
    th7.setAttribute('scope', 'row')
    th7.innerHTML = '3'

    var td7 = document.createElement('td')
    td7.innerHTML = 'Larry'

    var td8 = document.createElement('td')
    td8.innerHTML = 'the Bird'

    var td9 = document.createElement('td')
    td9.innerHTML = '@twitter'

    
    var Marktable = document.getElementById('maketable')
    Marktable.appendChild(mainTable)

    mainTable.appendChild(thead)
    mainTable.appendChild(tbody)

    thead.appendChild(tr1)
    tr1.appendChild(th2)
    tr1.appendChild(th3)
    tr1.appendChild(th4)

    tbody.appendChild(tr2)
    tbody.appendChild(tr3)
    tbody.appendChild(tr4)


    tr2.appendChild(th5)
    tr2.appendChild(td1)
    tr2.appendChild(td2)
    tr2.appendChild(td3)

    tr3.appendChild(th6)
    tr3.appendChild(td4)
    tr3.appendChild(td5)
    tr3.appendChild(td6)

    tr4.appendChild(th7)
    tr4.appendChild(td7)
    tr4.appendChild(td8)
    tr4.appendChild(td9)

}

function numberTable(){
    var showTable = document.getElementById('addTable')
    var formmain = document.createElement('table')
    formmain.setAttribute('id', 'TableA')

    var getNum = document.getElementById('getNumber').value
    var row = document.createElement("tr")
    var showtext =document.createElement('td')
    var text ='';
    for(var r = 0 ; r <= getNum; r++){
        text += r
        document.getElementById('TableA').appendChild(row)
        row.appendChild(showtext)
        showtext.innerHTML = text
        
      }
      
      showTable.appendChild(formmain)
    }
    function generateTable() {
    var showTable = document.getElementById("tableA")
    var trtd = document.getElementById("trc").value;

        // if(isNaN(trtd) || trtd < 1) {
        //     document.getElementById("table").innerHTML = "The given value must be a number!";
        // }
        // else {
            // var table = "<table>";
            var table ="";
            
            var formmain = document.createElement('table')
            
            var addtr = document.createElement('tr')
            var addtd = document.createElement('td')
            addtd.setAttribute('id', 'tdA') 
            showTable.appendChild(formmain)
            formmain.appendChild(addtr)
            addtr.appendChild(addtd)
            for(i = 1; i <=trtd; i++) {
                table  += i;
               

                           }
            
            // table += "</table>";
            document.getElementById("tdA").innerHTML = table;
        }
    // }